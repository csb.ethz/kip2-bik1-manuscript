function [minimum, rows, cols] = minMatrix(mat)
% Copyright (c) 2022 ETH Zurich, Lukas Widmer (l.widmer@gmail.com)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the MIT license
% See the LICENSE file or https://opensource.org/licenses/MIT for further information.
    [minimum, minimumIndex] = min(mat(:));
    
    [rows, cols] = ind2sub(size(mat), minimumIndex);
end