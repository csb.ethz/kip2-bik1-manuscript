function runEstimation(simFileNames)
% Copyright (c) 2022 ETH Zurich, Lukas Widmer (l.widmer@gmail.com)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the MIT license
% See the LICENSE file or https://opensource.org/licenses/MIT for further information.

    [parameterCombinations, simResultsAll, binsAll, expResults ] = loadExpDataAndSimResults(simFileNames); 
    
    debug = false;
    exportFigures = true;
    
    prefix = 'wt';
    expResultsToCompare = 15:20;
    expResultsNormalizeFrom = ones(size(expResultsToCompare));
    expResultsNormalizeToReference = expResultsNormalizeFrom;
    compareToDataFast(parameterCombinations, simResultsAll, binsAll, expResults, prefix, expResultsToCompare, expResultsNormalizeFrom, expResultsNormalizeToReference, debug, exportFigures);
    
    prefix = 'wt_may';
    expResultsToCompare = 98:103;
    expResultsNormalizeFrom = 11*ones(size(expResultsToCompare));
    compareToDataFast(parameterCombinations, simResultsAll, binsAll, expResults, prefix, expResultsToCompare, expResultsNormalizeFrom, expResultsNormalizeToReference, debug, exportFigures);
    
    prefix = 'wt_30';
    expResultsToCompare = 108:113;
    expResultsNormalizeFrom = 11*ones(size(expResultsToCompare));
    compareToDataFast(parameterCombinations, simResultsAll, binsAll, expResults, prefix, expResultsToCompare, expResultsNormalizeFrom, expResultsNormalizeToReference, debug, exportFigures);
        
    prefix = 'bik1';
    expResultsToCompare = 25:30;
    expResultsNormalizeFrom = ones(size(expResultsToCompare));
    compareToDataFast(parameterCombinations, simResultsAll, binsAll, expResults, prefix, expResultsToCompare, expResultsNormalizeFrom, expResultsNormalizeToReference, debug, exportFigures);
    
    prefix = 'deltaT';
    expResultsToCompare = 60:63;
    expResultsNormalizeFrom = ones(size(expResultsToCompare));    
    compareToDataFast(parameterCombinations, simResultsAll, binsAll, expResults, prefix, expResultsToCompare, expResultsNormalizeFrom, expResultsNormalizeToReference, debug, exportFigures);
end

