% Copyright (c) 2022 ETH Zurich, Lukas Widmer (l.widmer@gmail.com)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the MIT license
% See the LICENSE file or https://opensource.org/licenses/MIT for further information.

clear
close all
clc

myFolder = fileparts(mfilename('fullpath'));

exportPlots = true;

parameterEstimateResultFolder = [myFolder filesep '..' filesep '..' filesep '..' filesep 'simulationResults' filesep 'parameterEstimates'];
parameterEstimateFigureResultFolder = [myFolder filesep '..' filesep '..' filesep '..' filesep 'figures' filesep 'model'];

wtData = load([parameterEstimateResultFolder filesep 'wt_params.mat']);
bik1Data = load([parameterEstimateResultFolder filesep 'bik1_params.mat']);
deltaTData = load([parameterEstimateResultFolder filesep 'deltaT_params.mat']);

prefix = 'comparison-wt-bik1-deltaT';
widthFactor = 0.45;
pMin = min(wtData.parameterCombinations');
pMax = max(wtData.parameterCombinations');

%% Plot beeswarm plots
figure;
currAx = gca();
colors = currAx.ColorOrder;

concentrations.wt   = (wtData.plotSamples(1, :) ./ 140 .* 61);
concentrations.bik1 = (bik1Data.plotSamples(1, :) ./ 140 .* 61);
concentrations.deltaT = (deltaTData.plotSamples(1, :) ./ 140 .* 61);
[xConc, gConc] = addBoxPlotGroup([], [], concentrations.wt);
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, concentrations.bik1);
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, concentrations.deltaT);

violins = violinplot(xConc, gConc, 'Bandwidth', 30, 'Width', 0.4, 'ShowData', false);
violins(2).ViolinColor = {colors(2, :)};
violins(3).ViolinColor = {colors(3, :)};
xlim([0.5 3.5]);
ylabel('[Kip2]_{total} (nM)');
plot([0.6 3.4], [1 1] .* pMin(1) ./ 140 .* 61, '--k');
plot([0.6 3.4], [1 1] .* pMax(1) ./ 140 .* 61, '--k');
ylim([0 230]);

currAx.XTickLabel = {'WT', '\it{bik1\Delta}', 'Kip2-\DeltaT'};
currAx.XLabel.Color = [0 0 0];
currAx.YLabel.Color = [0 0 0];
currAx.XColor = [0 0 0];
currAx.YColor = [0 0 0];
xtickangle(45)
applyPaperFormatting

currFig = gcf();
currFig.Position(3) = widthFactor*currFig.Position(3);
currFig.Color = 'white';


fprintf('\nMean [Kip2]: %g,\tStd:%g\n', mean(concentrations.wt), std(concentrations.wt));
fprintf('Median [Kip2]: %g\n', median(concentrations.wt));

fprintf('Mean [Kip2 bik1del]: %g,\tStd:%g\n', mean(concentrations.bik1), std(concentrations.bik1));
fprintf('Median [Kip2 bik1del]: %g\n', median(concentrations.bik1));

fprintf('Mean [Kip2-deltaT]: %g,\tStd:%g\n', mean(concentrations.deltaT), std(concentrations.deltaT));
fprintf('Median [Kip2-deltaT]: %g\n', median(concentrations.deltaT));

if exportPlots
    print([parameterEstimateFigureResultFolder filesep 'pdf' filesep prefix '_violin_conc.pdf'], '-dpdf');
    export_fig([parameterEstimateFigureResultFolder filesep 'png' filesep prefix '_violin_conc.png'], '-q101', '-r300');
end

%%
k_out.wt   = (wtData.plotSamples(4, :));
k_out.bik1 = (bik1Data.plotSamples(4, :));
k_out.deltaT = (deltaTData.plotSamples(4, :));

figure;
fig1 = gcf();
fig1.Renderer='Painters';
xConc = [];
gConc = [];
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, k_out.wt);
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, k_out.bik1);
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, k_out.deltaT);

violins = violinplot(xConc, gConc, 'Bandwidth', 0.5, 'Width', 0.4, 'ShowData', false);
violins(2).ViolinColor = {colors(2, :)};
violins(3).ViolinColor = {colors(3, :)};


plot([0.6 3.4], [1 1] .* pMin(4), '--k');
plot([0.6 3.4], [1 1] .* pMax(4), '--k');

xlim([0.5 3.5]);
ylim([0.5 14]);
ylabel('k_{out} (s^{-1})');

xtickangle(45)
currAx = gca();
currAx.XTickLabel = {'WT', '\it{bik1\Delta}', 'Kip2-\DeltaT'};
currAx.XLabel.Color = [0 0 0];
currAx.YLabel.Color = [0 0 0];
currAx.XColor = [0 0 0];
currAx.YColor = [0 0 0];


applyPaperFormatting

currFig = gcf();
currFig.Position(3) = widthFactor*currFig.Position(3);
currFig.Color = 'white';

fprintf('\nMean k_out: %g\n', mean(k_out.wt));
fprintf('Median k_out: %g\n', median(k_out.wt));    

fprintf('Mean k_out Kip2 bik1del: %g\n', mean(k_out.bik1));
fprintf('Median k_out Kip2 bik1del: %g\n', median(k_out.bik1));    

fprintf('Mean k_out Kip2-deltaT: %g\n', mean(k_out.deltaT));
fprintf('Median k_out Kip2-deltaT: %g\n', median(k_out.deltaT)); 

if exportPlots
    print([parameterEstimateFigureResultFolder filesep 'pdf' filesep prefix '_violin_kout.pdf'], '-dpdf');
    export_fig([parameterEstimateFigureResultFolder filesep 'png' filesep prefix '_violin_kout.png'], '-q101', '-r300');
end


%%
k_on.wt     = (wtData.plotSamples(2, :));
k_on.bik1   = (bik1Data.plotSamples(2, :));
k_on.deltaT = (deltaTData.plotSamples(2, :));
figure;
fig1 = gcf();
fig1.Renderer='Painters';
xConc = [];
gConc = [];
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, log10(k_on.wt));
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, log10(k_on.bik1));
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, log10(k_on.deltaT));

k_in.wt       = (wtData.plotSamples(3, :));
k_in.bik1     = (bik1Data.plotSamples(3, :));
k_in.deltaT   = (deltaTData.plotSamples(3, :));

[xConc, gConc] = addBoxPlotGroup(xConc, gConc, log10(k_in.wt));
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, log10(k_in.bik1));
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, log10(k_in.deltaT));
violins = violinplot(xConc, gConc, 'Bandwidth', 1, 'Width', 0.4, 'ShowData', false);
violins(1).ViolinColor = {colors(1, :)};
violins(2).ViolinColor = {colors(2, :)};
violins(3).ViolinColor = {colors(3, :)};
violins(4).ViolinColor = {colors(1, :)};
violins(5).ViolinColor = {colors(2, :)};
violins(6).ViolinColor = {colors(3, :)};

plot([0.6 3.4], log10([1 1] .* pMin(2)), '--k');
plot([0.6 3.4], log10([1 1] .* pMax(2)), '--k');

plot([3.6 6.4], log10([1 1] .* pMin(3)), '--k');
plot([3.6 6.4], log10([1 1] .* pMax(3)), '--k');

xlim([0.5 6.5]);
ylim([-5.5 2.5]);
ylabel('nM Kip2^{-1} s^{-1}');

legend([violins(1).ViolinPlot, violins(2).ViolinPlot, violins(3).ViolinPlot], {'WT', '\it{bik1\Delta}', 'Kip2-\DeltaT'}, 'Location', 'NorthWest');
legend('boxoff');

currAx = gca();
currAx.XTickLabel = {'WT', '\it{bik1\Delta}',  'Kip2-\DeltaT'};
currAx.YTick = -5:5;
currAx.YTickLabel = {'10^{-5}', '10^{-4}', '10^{-3}', '10^{-2}', '10^{-1}', '10^{-0}', '10^{1}', '10^{2}', '10^{3}', '10^{4}', '10^{5}'};
currAx.XLabel.Color = [0 0 0];
currAx.YLabel.Color = [0 0 0];
currAx.XColor = [0 0 0];
currAx.YColor = [0 0 0];
xtickangle(45)
applyPaperFormatting

currFig = gcf();
currFig.Position(3) = 1.72*widthFactor*currFig.Position(3);
currFig.Color = 'white';

if exportPlots
    print([parameterEstimateFigureResultFolder filesep 'pdf' filesep prefix '_violin_kon_kin.pdf'], '-dpdf');
    export_fig([parameterEstimateFigureResultFolder filesep 'png' filesep prefix '_violin_kon_kin.png'], '-q101', '-r300');
end

fprintf('Mean k_on: %g,\tStd: %g\n', mean(k_on.wt), std(k_on.wt));
fprintf('Median k_on: %g\n', median(k_on.wt));

fprintf('Mean k_on Kip2 bik1del: %g,\tStd: %g\n', mean(k_on.bik1), std(k_on.bik1));
fprintf('Median k_on Kip2 bik1del: %g\n', median(k_on.bik1));

fprintf('Mean k_on Kip2-deltaT: %g,\tStd: %g\n', mean(k_on.deltaT), std(k_on.deltaT));
fprintf('Median k_on Kip2-deltaT: %g\n', median(k_on.deltaT));

fprintf('Mean k_in: %g,\tStd: %g\n', mean(k_in.wt), std(k_in.wt));
fprintf('Median k_in: %g\n', median(k_in.wt));

fprintf('Mean k_in Kip2 bik1del: %g,\tStd: %g\n', mean(k_in.bik1), std(k_in.bik1));
fprintf('Median k_in Kip2 bik1del: %g\n', median(k_in.bik1));

fprintf('Mean k_in Kip2-deltaT: %g,\tStd: %g\n', mean(k_in.deltaT), std(k_in.deltaT));
fprintf('Median k_in Kip2-deltaT: %g\n', median(k_in.deltaT));

%%
r_on.wt   = (wtData.plotSamples(2, :) .* wtData.plotSamples(1, :) ./ 140 .* 61);
r_on.bik1 = (bik1Data.plotSamples(2, :) .* bik1Data.plotSamples(1, :) ./ 140 .* 61);
r_on.deltaT = (deltaTData.plotSamples(2, :) .* deltaTData.plotSamples(1, :) ./ 140 .* 61);
figure;

xConc = [];
gConc = [];
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, log10(r_on.wt));
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, log10(r_on.bik1));
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, log10(r_on.deltaT));

r_in.wt   = (wtData.plotSamples(3, :) .* wtData.plotSamples(1, :) ./ 140 .* 61);
r_in.bik1 = (bik1Data.plotSamples(3, :) .* bik1Data.plotSamples(1, :) ./ 140 .* 61);
r_in.deltaT = (deltaTData.plotSamples(3, :) .* deltaTData.plotSamples(1, :) ./ 140 .* 61);

[xConc, gConc] = addBoxPlotGroup(xConc, gConc, log10(r_in.wt));
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, log10(r_in.bik1));
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, log10(r_in.deltaT));
violins = violinplot(xConc, gConc, 'Bandwidth', 1, 'Width', 0.4, 'ShowData', false);
violins(1).ViolinColor = {colors(1, :)};
violins(2).ViolinColor = {colors(2, :)};
violins(3).ViolinColor = {colors(3, :)};
violins(4).ViolinColor = {colors(1, :)};
violins(5).ViolinColor = {colors(2, :)};
violins(6).ViolinColor = {colors(3, :)};

plot([0.6 3.4], log10([1 1] .* pMin(2) .* pMin(1) ./ 140 .* 61), '--k');
plot([0.6 3.4], log10([1 1] .* pMax(2) .* pMax(1) ./ 140 .* 61), '--k');

plot([3.6 6.4], log10([1 1] .* pMin(3) .* pMin(1) ./ 140 .* 61), '--k');
plot([3.6 6.4], log10([1 1] .* pMax(3) .* pMax(1) ./ 140 .* 61), '--k');

xlim([0.5 6.5]);
ylim([-3.5 4.5]);
ylabel('s^{-1}');

legend([violins(1).ViolinPlot, violins(2).ViolinPlot, violins(3).ViolinPlot], {'WT', '\it{bik1\Delta}', 'Kip2-\DeltaT'}, 'Location', 'NorthWest');
legend('boxoff');

currAx = gca();
currAx.XTickLabel = {'WT', '\it{bik1\Delta}',  'Kip2-\DeltaT'};
currAx.YTick = -4:6;
currAx.YTickLabel = {'10^{-4}', '10^{-3}', '10^{-2}', '10^{-1}', '10^{0}', '10^{1}', '10^{2}', '10^{3}', '10^{4}', '10^{5}', '10^{6}'};
currAx.XLabel.Color = [0 0 0];
currAx.YLabel.Color = [0 0 0];
currAx.XColor = [0 0 0];
currAx.YColor = [0 0 0];
xtickangle(45)
applyPaperFormatting

currFig = gcf();
currFig.Position(3) = 1.72*widthFactor*currFig.Position(3);
currFig.Color = 'white';

if exportPlots
    print([parameterEstimateFigureResultFolder filesep 'pdf' filesep prefix '_violin_ron_rin.pdf'], '-dpdf');
    export_fig([parameterEstimateFigureResultFolder filesep 'png' filesep prefix '_violin_ron_rin.png'], '-q101', '-r300');
end

fprintf('Mean r_on: %g,\tStd: %g\n', mean(r_on.wt), std(r_on.wt));
fprintf('Median r_on: %g\n', median(r_on.wt));

fprintf('Mean r_on Kip2 bik1del: %g,\tStd: %g\n', mean(r_on.bik1), std(r_on.bik1));
fprintf('Median r_on Kip2 bik1del: %g\n', median(r_on.bik1));

fprintf('Mean r_on Kip2-deltaT: %g,\tStd: %g\n', mean(r_on.deltaT), std(r_on.deltaT));
fprintf('Median r_on Kip2-deltaT: %g\n', median(r_on.deltaT));

fprintf('Mean r_in: %g,\tStd: %g\n', mean(r_in.wt), std(r_in.wt));
fprintf('Median r_in: %g\n', median(r_in.wt));

fprintf('Mean r_in Kip2 bik1del: %g,\tStd: %g\n', mean(r_in.bik1), std(r_in.bik1));
fprintf('Median r_in Kip2 bik1del: %g\n', median(r_in.bik1));

fprintf('Mean k_in Kip2-deltaT: %g,\tStd: %g\n', mean(r_in.deltaT), std(r_in.deltaT));
fprintf('Median k_in Kip2-deltaT: %g\n', median(r_in.deltaT));

%%
wt_concentrations = unique(wtData.plotSamples(1, :));
bik1_concentrations = unique(bik1Data.plotSamples(1, :));
deltaT_concentrations = unique(deltaTData.plotSamples(1, :));

fprintf('Concentration difference: %g (H0: wt < bik1del)\n', mean(concentrations.wt > concentrations.bik1));
fprintf('Concentration difference: %g (H0: wt < deltaT)\n', mean(concentrations.wt > concentrations.deltaT));

%%
overlappingSamples = intersect(wt_concentrations, bik1_concentrations);
pValuesKon = zeros(size(overlappingSamples));
pValuesKin = zeros(size(overlappingSamples));
nSamples = zeros(size(overlappingSamples));

i = 1;
for conc = overlappingSamples
    wt_samples   = find(wtData.plotSamples(1, :) == conc);
    bik1_samples = find(bik1Data.plotSamples(1, :) == conc);
    
    wt_samples_rand   = wt_samples(randi(length(wt_samples), 1, 20000));
    bik1_samples_rand = bik1_samples(randi(length(bik1_samples), 1, 20000));
    
    pValuesKon(i) = mean(wtData.plotSamples(2, wt_samples_rand) >= bik1Data.plotSamples(2, bik1_samples_rand));
    pValuesKin(i) = mean(wtData.plotSamples(3, wt_samples_rand) >= bik1Data.plotSamples(3, bik1_samples_rand));
    nSamples(i) = length(wt_samples) + length(bik1_samples);
    i = i + 1;
end
fprintf('\nPer-concentration P(k_on,wt >= k_on,bik1del): ')
fprintf('%g, ', pValuesKon);
fprintf('\nPer-concentration P(k_in,wt >= k_in,bik1del): ')
fprintf('%g, ', pValuesKin);
fprintf('\nP(k_on,wt >= k_on,bik1del), overall: %g', sum(pValuesKon .* nSamples ./ sum(nSamples)))
fprintf('\nP(k_in,wt >= k_in,bik1del), overall: %g\n', sum(pValuesKin .* nSamples ./ sum(nSamples)))

%%
overlappingSamples = intersect(wt_concentrations, deltaT_concentrations);
pValuesKon = zeros(size(overlappingSamples));
pValuesKin = zeros(size(overlappingSamples));
nSamples = zeros(size(overlappingSamples));

i = 1;
for conc = overlappingSamples
    wt_samples   = find(wtData.plotSamples(1, :) == conc);
    deltaT_samples = find(deltaTData.plotSamples(1, :) == conc);
    
    wt_samples_rand   = wt_samples(randi(length(wt_samples), 1, 20000));
    bik1_samples_rand = deltaT_samples(randi(length(deltaT_samples), 1, 20000));
    
    pValuesKon(i) = mean(wtData.plotSamples(2, wt_samples_rand) >= deltaTData.plotSamples(2, bik1_samples_rand));
    pValuesKin(i) = mean(wtData.plotSamples(3, wt_samples_rand) >= deltaTData.plotSamples(3, bik1_samples_rand));
    nSamples(i) = length(wt_samples) + length(deltaT_samples);
    i = i + 1;
end
fprintf('\nPer-concentration P(k_on,wt >= k_on,deltaT): ')
fprintf('%g, ', pValuesKon);
fprintf('\nPer-concentration P(k_in,wt >= k_in,deltaT): ')
fprintf('%g, ', pValuesKin);
fprintf('\nP(k_on,wt >= k_on,deltaT), overall: %g', sum(pValuesKon .* nSamples ./ sum(nSamples)))
fprintf('\nP(k_in,wt >= k_in,deltaT), overall: %g\n', sum(pValuesKin .* nSamples ./ sum(nSamples)))
%%
fprintf('Median ratio k_on bik1del/wt: %g\n', median(k_on.bik1) ./ median(k_on.wt))
fprintf('Median ratio k_in bik1del/wt: %g\n', median(k_in.bik1) ./ median(k_in.wt))

fprintf('Median ratio k_on deltaT/wt: %g\n', median(k_on.deltaT) ./ median(k_on.wt))
fprintf('Median ratio k_in deltaT/wt: %g\n', median(k_in.deltaT) ./ median(k_in.wt))

%% WT out vs bik1-del out rate
fprintf('\n');
prob = mean(k_out.wt > k_out.bik1);
if prob == 0
    fprintf('WT out rate > deltaT out rate: p < %g \t(H0: k_out,bik1del < k_out,wt)\n', 1/length(k_out.wt));
else
    fprintf('WT out rate > deltaT out rate: p = %g \t(H0: k_out,bik1del < k_out,wt)\n', prob);
end
median(k_out.bik1) / median(k_out.wt)

%% WT out vs deltaT out rate
fprintf('\n');
prob = mean(k_out.wt > k_out.deltaT);
if prob == 0
    fprintf('WT out rate > deltaT out rate: p < %g \t(H0: k_out,deltaT < k_out,wt)\n', 1/length(k_out.wt));
else
    fprintf('WT out rate > deltaT out rate: p = %g \t(H0: k_out,deltaT < k_out,wt)\n', prob);
end
median(k_out.deltaT) / median(k_out.wt)

%%
mean(k_on.wt .* concentrations.wt >= k_on.bik1 .* concentrations.bik1)
mean(k_in.wt .* concentrations.wt >= k_in.bik1 .* concentrations.bik1)