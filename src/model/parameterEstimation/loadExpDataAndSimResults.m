function [parameterCombinations, simResultsAll, binsAll, expResults ] = loadExpDataAndSimResults(simFileNames)
% Copyright (c) 2022 ETH Zurich, Lukas Widmer (l.widmer@gmail.com)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the MIT license
% See the LICENSE file or https://opensource.org/licenses/MIT for further information.

%%
    myFolder = fileparts(mfilename('fullpath'));
    %%
    simResultFolder = [myFolder filesep '..' filesep '..' filesep '..' filesep 'simulationResults'];

    %%
    expResultFolder = [myFolder filesep '..' filesep '..' filesep '..' filesep 'analyzedData'];
    expResultFile = [expResultFolder filesep 'binnedProfiles.mat'];

    %% Load experimental results
    expResults = load(expResultFile);

    displayConditions(expResults.conditionResults);

    %%
    analyzedFileNames = simFileNames;
    for i = 1:length(simFileNames)
        analyzedFileNames{i} = ['analyzed-' analyzedFileNames{i}];
    end

    optimizationWorkspaces = {};
    simResultsAll = {};
    binsAll = [];

    %% Load simulation results
    for i = 1:length(simFileNames)
        optimizationWorkspaces{end+1} = load([simResultFolder filesep simFileNames{i}]);

        if optimizationWorkspaces{end}.runSingleDebug || optimizationWorkspaces{end}.runSingleParallel
            warning(['File "' simFileNames{i} '" contains incomplete parameter set since debug mode was active.']);
            if i == 1
                parameterCombinations = optimizationWorkspaces{i}.parameterCombinations(:, optimizationWorkspaces{end}.debugIndices);
            else
                parameterCombinations = [parameterCombinations optimizationWorkspaces{i}.parameterCombinations(:, optimizationWorkspaces{end}.debugIndices)];
            end
            simResults = load([simResultFolder filesep analyzedFileNames{i}]);
            simResultsAll = [simResultsAll simResults.results{optimizationWorkspaces{end}.debugIndices}];
        else
            if i == 1
                parameterCombinations = optimizationWorkspaces{i}.parameterCombinations;
            else
                parameterCombinations = [parameterCombinations optimizationWorkspaces{i}.parameterCombinations];
            end
            simResults = load([simResultFolder filesep analyzedFileNames{i}]);
            simResultsAll = [simResultsAll simResults.results];
        end

        binsAll = unique([binsAll; simResults.binsToCompute], 'rows');
    end
end