function plotEstimateErrorBar(parameterCombinations, uniqueCompatibleParameterSets, p, parameterIndex, x, color)


% Copyright (c) 2022 ETH Zurich, Lukas Widmer (l.widmer@gmail.com)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the MIT license
% See the LICENSE file or https://opensource.org/licenses/MIT for further information.

    plot(x, p(parameterIndex), 'x', 'Color', color, 'LineWidth', 0.8);
    if ~isempty(uniqueCompatibleParameterSets)
        uniqueParameterValues = unique(parameterCombinations(parameterIndex, uniqueCompatibleParameterSets));
        y = p(parameterIndex); 
        neg = y - uniqueParameterValues(1);
        pos = uniqueParameterValues(end) - y;
        errorbar(x,y,neg,pos,'Color', color, 'LineWidth', 0.8);
    end
end