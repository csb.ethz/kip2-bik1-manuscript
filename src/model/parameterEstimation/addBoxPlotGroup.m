function [x, g] = addBoxPlotGroup(x, g, x_new )

% Copyright (c) 2022 ETH Zurich, Lukas Widmer (l.widmer@gmail.com)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the MIT license
% See the LICENSE file or https://opensource.org/licenses/MIT for further information.

    if isempty(g)
        maxG = 1;
    else
        maxG = 1 + max(g);
    end
    x = [x; x_new];
    g = [g; zeros(size(x_new)) + maxG];
end

