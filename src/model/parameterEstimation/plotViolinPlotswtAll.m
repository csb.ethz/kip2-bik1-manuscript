% Copyright (c) 2022 ETH Zurich, Lukas Widmer (l.widmer@gmail.com)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the MIT license
% See the LICENSE file or https://opensource.org/licenses/MIT for further information.

clear
close all
clc

myFolder = fileparts(mfilename('fullpath'));

exportPlots = true;

parameterEstimateResultFolder = [myFolder filesep '..' filesep '..' filesep '..' filesep 'simulationResults' filesep 'parameterEstimates'];
parameterEstimateFigureResultFolder = [myFolder filesep '..' filesep '..' filesep '..' filesep 'figures' filesep 'model'];

wtData = load([parameterEstimateResultFolder filesep 'wt_params.mat']);
wt_may_Data = load([parameterEstimateResultFolder filesep 'wt_may_params.mat']);
wt_30_Data = load([parameterEstimateResultFolder filesep 'wt_30_params.mat']);

prefix = 'comparison-wt';

pMin = min(wtData.parameterCombinations');
pMax = max(wtData.parameterCombinations');

%% Plot beeswarm plots
figure;
currAx = gca();
colors = currAx.ColorOrder;

concentrations.wt   = (wtData.plotSamples(1, :) ./ 140 .* 61);
concentrations.may_Data = (wt_may_Data.plotSamples(1, :) ./ 140 .* 61);
concentrations.wt_30_data = (wt_30_Data.plotSamples(1, :) ./ 140 .* 61);
[xConc, gConc] = addBoxPlotGroup([], [], concentrations.wt);
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, concentrations.may_Data);
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, concentrations.wt_30_data);

violins = violinplot(xConc, gConc, 'Bandwidth', 30, 'Width', 0.4, 'ShowData', false);
violins(2).ViolinColor = {colors(2, :)};
violins(3).ViolinColor = {colors(3, :)};
xlim([0.5 3.5]);
ylabel('[Kip2]_{total} (nM)');
plot([0.6 3.4], [1 1] .* pMin(1) ./ 140 .* 61, '--k');
plot([0.6 3.4], [1 1] .* pMax(1) ./ 140 .* 61, '--k');
ylim([0 230]);

currAx.XTickLabel = {'WT', 'WT (05-14)', 'WT (05-14, 30 �C)'};
currAx.XLabel.Color = [0 0 0];
currAx.YLabel.Color = [0 0 0];
currAx.XColor = [0 0 0];
currAx.YColor = [0 0 0];
xtickangle(45)
applyPaperFormatting

currFig = gcf();
currFig.Position(3) = 0.55*currFig.Position(3);
currFig.Color = 'white';


fprintf('\nMean [Kip2]: %g,\tStd:%g\n', mean(concentrations.wt), std(concentrations.wt));
fprintf('Median [Kip2]: %g\n', median(concentrations.wt));

fprintf('Mean [Kip2-may_Data]: %g,\tStd:%g\n', mean(concentrations.may_Data), std(concentrations.may_Data));
fprintf('Median [Kip2-may_Data]: %g\n', median(concentrations.may_Data));

fprintf('Mean [Kip2 wt_30_data]: %g,\tStd:%g\n', mean(concentrations.wt_30_data), std(concentrations.wt_30_data));
fprintf('Median [Kip2 wt_30_data]: %g\n', median(concentrations.wt_30_data));

if exportPlots
    print([parameterEstimateFigureResultFolder filesep 'pdf' filesep prefix '_violin_conc.pdf'], '-dpdf');
    export_fig([parameterEstimateFigureResultFolder filesep 'png' filesep prefix '_violin_conc.png'], '-q101', '-r300');
end

%%
k_out.wt         = (wtData.plotSamples(4, :));
k_out.may_Data   = (wt_may_Data.plotSamples(4, :));
k_out.wt_30_data = (wt_30_Data.plotSamples(4, :));

figure;
xConc = [];
gConc = [];
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, k_out.wt);
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, k_out.may_Data);
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, k_out.wt_30_data);

violins = violinplot(xConc, gConc, 'Bandwidth', 0.5, 'Width', 0.4, 'ShowData', false);
violins(2).ViolinColor = {colors(2, :)};
violins(3).ViolinColor = {colors(3, :)};


plot([0.6 3.4], [1 1] .* pMin(4), '--k');
plot([0.6 3.4], [1 1] .* pMax(4), '--k');

xlim([0.5 3.5]);
ylim([0.5 14]);
ylabel('k_{out} (s^{-1})');

xtickangle(45)
currAx = gca();
currAx.XTickLabel = {'WT', 'WT (05-14)', 'WT (05-14, 30 �C)'};
currAx.XLabel.Color = [0 0 0];
currAx.YLabel.Color = [0 0 0];
currAx.XColor = [0 0 0];
currAx.YColor = [0 0 0];


applyPaperFormatting

currFig = gcf();
currFig.Position(3) = 0.55*currFig.Position(3);
currFig.Color = 'white';

fprintf('\nMean k_out: %g\n', mean(k_out.wt));
fprintf('Median k_out: %g\n', median(k_out.wt));    

fprintf('Mean k_out may_Data: %g\n', mean(k_out.may_Data));
fprintf('Median k_out may_Data: %g\n', median(k_out.may_Data));    

fprintf('Mean k_out wt_30_data: %g\n', mean(k_out.wt_30_data));
fprintf('Median k_out wt_30_data: %g\n', median(k_out.wt_30_data)); 

if exportPlots
    print([parameterEstimateFigureResultFolder filesep 'pdf' filesep prefix '_violin_kout.pdf'], '-dpdf');
    export_fig([parameterEstimateFigureResultFolder filesep 'png' filesep prefix '_violin_kout.png'], '-q101', '-r300');
end
%%
k_on.wt         = (wtData.plotSamples(2, :));
k_on.may_Data   = (wt_may_Data.plotSamples(2, :));
k_on.wt_30_data = (wt_30_Data.plotSamples(2, :));
figure;

xConc = [];
gConc = [];
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, log10(k_on.wt));
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, log10(k_on.may_Data));
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, log10(k_on.wt_30_data));


violins = violinplot(xConc, gConc, 'Bandwidth', 1, 'Width', 0.4, 'ShowData', false);
violins(2).ViolinColor = {colors(2, :)};
violins(3).ViolinColor = {colors(3, :)};
plot([0.6 3.4], log10([1 1] .* pMin(2)), '--k');
plot([0.6 3.4], log10([1 1] .* pMax(2)), '--k');

xlim([0.5 3.5]);
ylim([-5.5 2.5]);
ylabel('k_{on} (nM Kip2^{-1} s^{-1})');

%legend([violins(1).ViolinPlot, violins(2).ViolinPlot], {'WT', 'Kip2-may_Data'}, 'Location', 'NorthWest');
%legend('boxoff');

currAx = gca();
currAx.XTickLabel = {'WT', 'WT (05-14)', 'WT (05-14, 30 �C)'};
currAx.YTick = -5:5;
currAx.YTickLabel = {'10^{-5}', '10^{-4}', '10^{-3}', '10^{-2}', '10^{-1}', '10^{-0}', '10^{1}', '10^{2}', '10^{3}', '10^{4}', '10^{5}'};
currAx.XLabel.Color = [0 0 0];
currAx.YLabel.Color = [0 0 0];
currAx.XColor = [0 0 0];
currAx.YColor = [0 0 0];
xtickangle(45)
applyPaperFormatting

currFig = gcf();
currFig.Position(3) = 0.7*currFig.Position(3);
currFig.Color = 'white';

if exportPlots
    print([parameterEstimateFigureResultFolder filesep 'pdf' filesep prefix '_violin_kon.pdf'], '-dpdf');
    export_fig([parameterEstimateFigureResultFolder filesep 'png' filesep prefix '_violin_kon.png'], '-q101', '-r300');
end

%%

k_in.wt         = (wtData.plotSamples(3, :));
k_in.may_Data   = (wt_may_Data.plotSamples(3, :));
k_in.wt_30_data = (wt_30_Data.plotSamples(3, :));

figure;

xConc = [];
gConc = [];
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, log10(k_in.wt));
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, log10(k_in.may_Data));
[xConc, gConc] = addBoxPlotGroup(xConc, gConc, log10(k_in.wt_30_data));


violins = violinplot(xConc, gConc, 'Bandwidth', 1, 'Width', 0.4, 'ShowData', false);
violins(2).ViolinColor = {colors(2, :)};
violins(3).ViolinColor = {colors(3, :)};
plot([0.6 3.4], log10([1 1] .* pMin(3)), '--k');
plot([0.6 3.4], log10([1 1] .* pMax(3)), '--k');

xlim([0.5 3.5]);
ylim([-5.5 2.5]);
ylabel('k_{in} (nM Kip2^{-1} s^{-1})');

%legend([violins(1).ViolinPlot, violins(2).ViolinPlot], {'WT', 'Kip2-may_Data'}, 'Location', 'NorthWest');
%legend('boxoff');

currAx = gca();
currAx.XTickLabel = {'WT', 'WT (05-14)', 'WT (05-14, 30 �C)'};
currAx.YTick = -5:5;
currAx.YTickLabel = {'10^{-5}', '10^{-4}', '10^{-3}', '10^{-2}', '10^{-1}', '10^{-0}', '10^{1}', '10^{2}', '10^{3}', '10^{4}', '10^{5}'};
currAx.XLabel.Color = [0 0 0];
currAx.YLabel.Color = [0 0 0];
currAx.XColor = [0 0 0];
currAx.YColor = [0 0 0];
xtickangle(45)
applyPaperFormatting

currFig = gcf();
currFig.Position(3) = 0.7*currFig.Position(3);
currFig.Color = 'white';

fprintf('Mean k_in: %g,\tStd: %g\n', mean(k_in.wt), std(k_in.wt));
fprintf('Median k_in: %g\n', median(k_in.wt));

fprintf('Mean k_in may_Data: %g,\tStd: %g\n', mean(k_in.may_Data), std(k_in.may_Data));
fprintf('Median k_in may_Data: %g\n', median(k_in.may_Data));

if exportPlots
    print([parameterEstimateFigureResultFolder filesep 'pdf' filesep prefix '_violin_kin.pdf'], '-dpdf');
    export_fig([parameterEstimateFigureResultFolder filesep 'png' filesep prefix '_violin_kin.png'], '-q101', '-r300');
end