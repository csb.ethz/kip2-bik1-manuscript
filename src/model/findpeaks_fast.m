function [Xpk, Ypk] = findpeaks_fast(Yin)
% Copyright (c) 2022 ETH Zurich, Lukas Widmer (l.widmer@gmail.com)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the MIT license
% See the LICENSE file or https://opensource.org/licenses/MIT for further information.

pp1 = [Yin(1) Yin(1:end-1)];
pp2 = [Yin(2:end) Yin(end)];


Xpk = find((Yin > pp1) & (Yin > pp2));

if nargin > 1
    Ypk = Yin(Xpk);
end
