function parsave(fname, result)

% Copyright (c) 2022 ETH Zurich, Lukas Widmer (l.widmer@gmail.com)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the MIT license
% See the LICENSE file or https://opensource.org/licenses/MIT for further information.

    if isempty(fname)
        %warning('No filename specified - not saving file');
        return
    end
    save(fname, 'result', '-v7.3');
end