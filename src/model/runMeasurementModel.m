function result = runMeasurementModel(absResultFolder, parameterIndex, currentParameters, tspan)

% Copyright (c) 2022 ETH Zurich, Lukas Widmer (l.widmer@gmail.com)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the MIT license
% See the LICENSE file or https://opensource.org/licenses/MIT for further information.

    if absResultFolder(end) ~= filesep
        absResultFolder = [absResultFolder filesep];
    end
    absVmResultFolder = [absResultFolder 'vm' filesep];

    currentFileName = [absResultFolder int2str(parameterIndex) '.mat'];
    if ~exist(currentFileName, 'file')
        result = simulateModel(currentParameters, parameterIndex, tspan, currentFileName);
    end
    
    currentOutFileName = [absVmResultFolder int2str(parameterIndex) '.mat'];
    if ~exist(currentOutFileName, 'file')
        if isfield(currentParameters, 'binSizeLimit')
            result = computeMeasurementModel(result, currentOutFileName, currentParameters.binSizeLimit);
        else
            result = computeMeasurementModel(result, currentOutFileName);
        end
    end
end

