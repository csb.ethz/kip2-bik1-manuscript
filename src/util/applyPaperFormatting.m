function applyPaperFormatting()
% applyPaperFormatting: Make everything Helvetica, font size 12

% Copyright (c) 2022 ETH Zurich, Lukas Widmer (l.widmer@gmail.com)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the MIT license
% See the LICENSE file or https://opensource.org/licenses/MIT for further information.

    fig = gcf;
    set(findall(fig, '-property', 'FontSize'), 'FontSize', 12) 
    set(findall(fig, '-property', 'FontName'), 'FontName', 'Helvetica')
end