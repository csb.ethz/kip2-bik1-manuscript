function exportTwoColorCondtionResultToData(conditionResult, fileName)
% Copyright (c) 2022 ETH Zurich, Lukas Widmer (l.widmer@gmail.com)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the MIT license
% See the LICENSE file or https://opensource.org/licenses/MIT for further information.

    dataTableGreen = array2table([conditionResult.lengthVector conditionResult.greenIntensities]);
    dataTableGreen.Properties.VariableNames{1} = 'x';
    dataTableGreen.Properties.VariableNames(2:end) = conditionResult.cellNames;
    writetable(dataTableGreen, [fileName '-green.txt'], 'Delimiter', '\t')
    
    dataTableRed = array2table([conditionResult.lengthVector conditionResult.redIntensities]);
    dataTableRed.Properties.VariableNames{1} = 'x';
    dataTableRed.Properties.VariableNames(2:end) = conditionResult.cellNames;
    writetable(dataTableRed, [fileName '-red.txt'], 'Delimiter', '\t')
end

