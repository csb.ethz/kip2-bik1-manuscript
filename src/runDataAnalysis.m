% runDataAnalysis: Runs data analysis and plots profile figures, as well as
% figures related to the model.
%
%   Usage:
%   runDataAnalysis()

% Copyright (c) 2022 ETH Zurich, Lukas Widmer (l.widmer@gmail.com)
% Computational Systems Biology group, D-BSSE
%
% This software is freely available under the MIT license
% See the LICENSE file or https://opensource.org/licenses/MIT for further information.

%% 1. Download & load dependencies
loadDependencies();

%% 2. Aggregate red/green profile CSV files into tables
aggregateData();

%% 3. Perform peak detection, compute lengths, and bin data
lengthAnalysis();

%% 4. Plot binned data
compareConditions();

%% 5. Simulate model & compute measurement model
% This is computationally intense and requires cluster usage.
% To run this, open and run src/modelFitting/runSampling.m and adjust the
% line below with your results file
simFileNames = {
     'simResults-20190221T061021.mat'
};
%%
addpath(['model']);
addpath(['model' filesep 'parameterEstimation']);

%% 6. Compare simulation results to data, and estimate parameters
compareToDataFastRestrictedAll(); % Optimize B parameter
runEstimation(simFileNames);

%% 7. Export comparison plots
plotViolinPlotswtAll();
plotViolinPlotswtbik1deltaT();

%% 8. Export kymograph with off rate from Hibbel et. al. (2015).
plotKymograph();