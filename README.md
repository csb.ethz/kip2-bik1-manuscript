Info
=====
Raw data and scripts used for data analysis and modelling in [[1]], builds on previous work published in [[2]] (and repository [[3]]).

License
-------
Copyright (c) 2023 ETH Zurich, Lukas Widmer (l.widmer@gmail.com)  
Computational Systems Biology group, D-BSSE

This software is freely available under the MIT license.  
See the [LICENSE](LICENSE) file or https://opensource.org/licenses/MIT for further information.

Authors
-------

    Lukas Widmer <l.widmer@gmail.com> (Scripts, Data Analysis)
    Xiuzhen Chen <xiuzhen.chen@bc.biol.ethz.ch> (Image Data)


Installation & Usage
====================

Windows
-------
1. Install MATLAB (tested on R2016b-R2018a)
2. Install Git via TortoiseGit (https://tortoisegit.org/)
3. Git clone this repo (recursively)

Mac OS X
--------
1. Install MATLAB
2. Install Git through package manager: e.g., `brew install git`
3. Git clone this repo (recursively)

Linux (instructions here are for Ubuntu / Debian)
-------------------------------------------------
1. Install MATLAB
2. Install Git through package manager: `sudo apt-get install git`
3. Git clone this repo (recursively)

Usage
=====
* Run [runDataAnalysis.m](src/runDataAnalysis.m) in MATLAB to perform the analysis of the experimental data, and generate the data and model figures.
* Run [runSampling.m](src/model/runSampling.m) with one of the modes specified therein to simulate the stochastic model and compute the measurement model. Warning: this is computationally intense, using a compute cluster is highly recommended.

References
==========
[1]: https://doi.org/10.1083/jcb.202110126
[2]: https://doi.org/10.7554/eLife.48627
[3]: https://gitlab.com/csb.ethz/Kip2-SPB-Profile-Manuscript
1. Xiuzhen Chen, Didier Portran†, Lukas A. Widmer†, Marcel M. Stangier†, Dimitris Liakopoulos, Jörg Stelling, Michel O. Steinmetz, Yves Barral\* (2023)  
   The motor domain of the kinesin Kip2 promotes microtubule polymerization at microtubule tips  
   \* corresponding author: <yves.barral@bc.biol.ethz.ch>  
   JCB, 2023 
2. Xiuzhen Chen†, Lukas A. Widmer†, Marcel M. Stangier, Michel O. Steinmetz, Jörg Stelling*, Yves Barral*. (2019)  
   Remote control of microtubule plus-end dynamics and function from the minus-end  
  \* corresponding authors: <joerg.stelling@bsse.ethz.ch>, <yves.barral@bc.biol.ethz.ch>  
   eLife, 2019 
3. GitLab repository for ref. 2

† contributed equally  
